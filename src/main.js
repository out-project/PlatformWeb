import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import './http'
import qs from 'qs'
import Axios from 'axios'
import ws from './utils/ws'
import moment from 'moment'
import VueElementExtends from 'vue-element-extends'
import 'vue-element-extends/lib/index.css'
import scroll from 'vue-seamless-scroll'
import 'mint-ui/lib/style.css'
import Minit from 'mint-ui'

Vue.use(Minit);
Vue.use(scroll);
Vue.prototype.$moment = moment;//时间格式化
window.echarts = require("echarts");
Vue.use(ElementUI);
Vue.use(VueElementExtends);//可编辑表格
Vue.config.productionTip = false;
Axios.interceptors.response.use(function (response) {
        console.log("response:"+response.toString());
        return response
    },function (error) {
         console.log(error)
        if (error.hasOwnProperty("response")){
            console.log(error,"******");
            if (error.response.status === 302){
                ElementUI.Notification.error({
                    title:'302',
                    message:'response.data'
                });
                localStorage.clear();
                // router.replace({path:'/login',query:{redirect:router.currentRoute.fullPath}});
            }else if (error.response.status === 400) {
                ElementUI.Notification.warning({
                    title:'警告',
                    message:error.response.data.message
                });
            } else if(error.response.status === 403){
                ElementUI.Notification.error({
                    title:'无权访问',
                    message:'您无权访问该资源!'
                });
            }else if (error.response.status === 404) {
                ElementUI.Notification.error({
                    title:'404',
                    message:'未找到你想要的资源!'
                });
            } else if (error.response.status === 500) {
                ElementUI.Notification.error({
                    title:'内部错误',
                    message:error.response.data.message
                });
            }else {
                ElementUI.Notification.error({
                    title:'请求错误',
                    message:'Network Error'
                });
            }
        } else {
            ElementUI.Notification.error({
                title:'请求错误',
                message:'Network Error'
            });
        }
        console.log("错误:"+error);

        return error;
});
Axios.interceptors.request.use(function (request) {
    console.log(request);
    return request;
});

Vue.prototype.$qs = qs;
Vue.prototype.$ws = ws;
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
