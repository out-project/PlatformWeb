import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    socket:Object
  },
  mutations: {
    setSocket (state,value){
      state.socket = value;
      localStorage.setItem('socket',value)
    }
  },
  actions: {
  },
  getters: {
    getSocket(state){
      return state.socket;
    }
  }
})
