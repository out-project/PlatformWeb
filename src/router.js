import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

function route (path, file, name, children) {
    return {
        exact: true,
        path,
        name,
        children,
        component: () => import('./views' + file)
    }
}


export default new Router({
    routes: [
        route("/login",'/Login',"Login"),// /login路径，路由到登录组件
        {
            path:"/", // 根路径，路由到 Layout组件
            component: () => import('./views/Layout'),
            redirect:"/login",
            children:[ // 其它所有组件都是 Layout的子组件。
                route("/home","/Home","Home"),
                route("/system/permission","/system/permission/index","Permission"),
                route("/system/user","/system/user/UserManager","UserManager"),
                route("/system/user","/system/user/UserManager","UserManager"),
                route("/system/file","/system/file/FileManage","FileManage"),
                route("/system/role","/system/role/Role","Role"),
                route("/system/code","/system/code/Code","code"),

                route("/userInfo/mine","/userInfo/Mine","Mine"),
                route("/userInfo/car","/userInfo/Car","Car"),
                route("/logger","/log/Logger","Logger"),
                route("/message/now","/comments/New","New"),
                route("/message/history","/comments/Old","Old"),
                route("/swagger","/swagger/Doc","Doc"),

                route("/record","/record/RecordList","List"),
                route("/plateNum","/plateNum/PlateNum","plateNum"),
                route("/parking/list","/parking/List","List"),
                route("/parking/edit","/parking/Edit","Edit"),
                route("/parking/carPlaceList","/parking/CarPlaceList","carPlaceList"),
                route("/parking/payRuleList","/parking/PayRuleList","payRuleList"),

                route("/order/now","/order/now/orderIndex","order"),
                route("/order/history","/order/history/history","history"),

                route("/info","/infomation/Info","Info"),
                route("/test","/Test","test")
            ]
        },

        {
            path:"/app",
            component: () => import('./views/userApp/home'),
            redirect:"/app/home",
            children:[
                route("/app/home",'/userApp/order'),
                route("/app/center",'/userApp/center'),
                route("/app/info",'/userApp/info'),
            ]
        },
    ]
})
