export default {
    async getOptionsByCode(data){
        let _this = this;
        return new Promise(resolve => {
            _this.$http.get("/coder/getOptionsByCode/"+data).then(res=>{
                resolve(res)
            })
        }).catch(err=>{
            _this.$message.error("访问出现异常")
        })
    }
}
