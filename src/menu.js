var menus = [
    {
        "id": 1,
        "name": "主页",
        "type": 0,
        "permission": "update",
        "url": "/home",
        "icon":"el-icon-setting",
        "children": [
            {
                "id": 1,
                "name": "新增",
                "type": 1,
                "permission": "update",
                "url": "/home",
                "icon": "",
                "children": []
            }
        ]
    },
    {
        "id": 1,
        "name": "停车场管理",
        "type": 0,
        "permission": "update",
        "url": "/parking",
        "icon":"el-icon-setting",
        "children": [
            {
                "id": 1,
                "name": "车库列表",
                "type": 0,
                "permission": "update",
                "url": "/list",
                "icon":"",
                "children": []
            },
            {
                "id": 1,
                "name": "车库详情",
                "type": 0,
                "permission": "update",
                "url": "/detail",
                "icon":"",
                "children": []
            }
        ]
    },
    {
        "id": 1,
        "name": "订单管理",
        "type": 0,
        "permission": "update",
        "url": "/order",
        "icon":"el-icon-setting",
        "children": [
            {
                "id": 1,
                "name": "预定订单",
                "type": 0,
                "permission": "update",
                "url": "/bookOrder",
                "icon":"",
                "children": []
            },
            {
                "id": 1,
                "name": "历史订单",
                "type": 0,
                "permission": "update",
                "url": "/history",
                "icon":"",
                "children": []
            },
        ]
    },
    {
        "id": 1,
        "name": "车库记录表",
        "type": 0,
        "permission": "update",
        "url": "/record",
        "icon":"el-icon-setting",
        "children": []
    },
    {
        "id": 1,
        "name": "留言管理",
        "type": 0,
        "permission": "update",
        "url": "/message",
        "icon":"el-icon-setting",
        "children": [
            {
                "id": 1,
                "name": "最新留言",
                "type": 0,
                "permission": "update",
                "url": "/now",
                "icon":"",
                "children": []
            },
            {
                "id": 1,
                "name": "历史留言",
                "type": 0,
                "permission": "update",
                "url": "/history",
                "icon":"",
                "children": []
            }
        ]
    },
    {
        "id": 1,
        "name": "车辆管理",
        "type": 0,
        "permission": "update",
        "url": "/car",
        "icon":"el-icon-setting",
        "children": [
            {
                "id": 1,
                "name": "我的车辆",
                "type": 0,
                "permission": "update",
                "url": "/mine",
                "icon":"",
                "children": []
            }
        ]
    },
    {
        "id": 1,
        "name": "app展示",
        "type": 0,
        "permission": "update",
        "url": "/app",
        "icon":"el-icon-setting",
        "children": []
    },
    {
        "id": 1,
        "name": "系统管理",
        "type": 0,
        "permission": "update",
        "url": "/system",
        "icon":"el-icon-setting",
        "children": [
            {
                "id": 1,
                "name": "用户管理",
                "type": 0,
                "permission": "update",
                "url": "/system/user",
                "icon":"",
                "children": []
            },
            {
                "id": 1,
                "name": "角色管理",
                "type": 0,
                "permission": "update",
                "url": "/system/role",
                "icon":"",
                "children": []
            },
            {
                "id": 1,
                "name": "权限管理",
                "type": 0,
                "permission": "update",
                "url": "/system/permission",
                "icon":"",
                "children": []
            },
            {
                "id": 1,
                "name": "文件管理",
                "type": 0,
                "permission": "update",
                "url": "/system/file",
                "icon":"",
                "children": []
            },
            {
                "id": 1,
                "name": "码表管理",
                "type": 0,
                "permission": "update",
                "url": "/system/code",
                "icon":"",
                "children": []
            },
        ]
    },


]

export default menus