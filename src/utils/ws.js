import {Loading} from 'element-ui'

function DataViewExt (buffer) {
    let dv = new DataView(buffer, 0);
    dv.setString = function (offset, string) {
        if (typeof string === 'string'){
            for (let i = 0; i < string.length; i++) {
                let c = string.charCodeAt(i);
                this.setUint8(offset + i, c)
            }
        }
    };
    dv.setBinary = function (offset, binary) {
        for (let i = 0; i < binary.length; i++) {
            this.setUint8(offset + i, binary[i])
        }
    };
    return dv
}

export default {
    loadingInstance:'',
    loadingOptions:{
        lock:true,
        text:'WebSocket连接断开，正在重连...',
        spinner: 'el-icon-loading',
        background:'rgba(0,0,0,0.8)'
    },
    num: 0,
    //创建socket实例
    initWebSocket(callback){
        let self = this;
        let api = "ws://localhost:8088/ws";
        const socket = new WebSocket(api);
        socket.onopen = function () {
            let reconnectTimer = null;
            //尚未建立连接
            if (socket.readyState === 0){
                if (reconnectTimer){
                    clearTimeout(reconnectTimer);
                }
                reconnectTimer = setTimeout(function () {
                    self.initWebSocket(callback);
                    reconnectTimer = null
                },500)
            }
            //已经建立连接，可以进行通信
            if (socket.readyState === 1){
                console.log("%cWebSocket 连接成功"," text-shadow: 0 1px 0 #ccc,0 2px 0 #c9c9c9,0 3px 0 #bbb,0 4px 0 #b9b9b9,0 5px 0 #aaa,0 6px 1px rgba(0,0,0,.1),0 0 5px rgba(0,0,0,.1),0 1px 3px rgba(0,0,0,.3),0 3px 5px rgba(0,0,0,.2),0 5px 10px rgba(0,0,0,.25),0 10px 10px rgba(0,0,0,.2),0 20px 20px rgba(0,0,0,.15);font-size:3em")
                if (self.loadingInstance){
                    self.loadingInstance.close()
                }
                callback(socket)
            }
        };
        socket.onmessage = function (response) {
            console.log("客户端接收到的消息！");
            if (typeof response.data === 'string'){
                console.log("客户端接收到的消息:",response.data)
            }
        };
        socket.onerror = function(){
            this.$notify.warning("socket连接出现异常");
            console.log("socket连接出现异常")
        };
//        // socket关闭时触发
//        socket.onclose = function () {
//            self.reconnect(callback)        }
    },
    //请求数据
    sendKey(socket,key){
        let self = this;
        if (socket.readyState === 0){
            setTimeout(function () {
                self.sendKey(socket,key)
            },300)
        }
        if (socket.readyState === 1){
            socket.send(key)
        }

    },
    //发送设备数据
    sendData(socket,data){
        let self = this;
        if (socket.readyState === 0){
            setTimeout(function () {
                self.sendData(socket,data)
            },300)
        }
        if (socket.readyState === 1){
            let buffer = new ArrayBuffer(data.length);
            let dv = new DataViewExt(buffer);
            dv.setBinary(0,data);
            socket.send(buffer)
        }

    },

    //发送对象给服务端
    sendObject(socket,object){
        if (socket.readyState === 0){
           console.log("websocke断开")
        }
        if (socket.readyState === 1){
            if (object){
                socket.send(JSON.stringify(object));
            }
        }

    },

    //重新连接
    reconnect(url,callback){
        let windowUrl = window.location.href;
        let path = windowUrl.slice(windowUrl.indexOf('#')+1,windowUrl.length);
        if (this.num > 2){
            window.location.href = 'http://127.0.0.1:8081'
        }
        this.num ++;
        if (path === '/'){
            return false;
        }
        this.loadingInstance = Loading.service(this.loadingOptions);
        this.initWebSocket(url,callback)
    }





}