import Axios from 'axios'
Axios.defaults.withCredentials = true; //允许请求携带cookie
Axios.defaults.baseURL = "http://localhost:8088/api"; // 设置axios的基础请求路径
Axios.defaults.timeout = 2000; // 设置axios的请求时间
import moment from 'moment'
export default {
    /**
     * @return {string}
     */
    RoleFilter(data){
        let roleData = JSON.parse(localStorage.getItem("roles"));
        let roleName = "";
        roleData.forEach(r => {
            if (r.id === data){
                roleName = r.remark;
            }
        });
        return roleName;
    },

    /**
     * @return {string}
     */
    UserNameFilter(data){
        let userData = JSON.parse(localStorage.getItem("users"));
        let userName = "";
        userData.forEach(r => {
            if (r.id === data){
                userName = r.userName;
            }
        });
        return userName;
    },

    /**
     * @return {string}
     */
    CodeFilter(data){
        let codeData = JSON.parse(localStorage.getItem("coders"));
        let code = "";
        codeData.forEach(c => {
            if (c.id === data){
                code = c.valueDesc;
            }
        });
        return code;
    },

    /**
     * @return {string}
     */
    DateFilter(data){
        if (data==null){
            return "---"
        } else {
            return moment(data).format("MM月DD日 HH:mm")
        }
    },


    ParkingFilter(data){
        let name = "";
        return new Promise((resolve, reject) => {
            Axios.get("/parking/get/"+data).then(res=>{
                if (res.status === 200){
                    name = res.data.parkingName;
                    resolve(name)
                }else {
                    resolve(name)
                }
            });
        });
    },

  /**
   * @return {string}
   */
  PayRulerFilter(data){
      let ruleData = JSON.parse(localStorage.getItem("rules"));
      let rule = "";
      ruleData.forEach(c => {
        if (c.id === data){
          rule = c.payRule;
        }
      });
      return rule;
    }

}