var path = require("path");
module.exports = {
    filenameHashing: true,
    // webpack配置
    devServer: {
        open: true,
        port: 8082,
        host: '0.0.0.0',
        proxy: {
            '/api': {
               target: 'http://localhost:8088',
                ws: true,
                changeOrigin: true,
                pathRewrite: {
                    '^/api/': '/api/'
                }
            },
        }
    },
    configureWebpack:function(config){
        const externals = {
            AMap: "AMap"
        };
    },
    chainWebpack:function (config) {
        config.plugin("html").tap(args => {
            const cdn = {
                // 开发环境
                dev: {
                    css: [],
                    js: [
                        "https://webapi.amap.com/maps?v=1.4.13&key=209e57a8bf80dd59da2650f389a2b6c5",
                        "//webapi.amap.com/ui/1.0/main.js"
                    ]
                },
                build: {
                    css: [],
                    js: [
                        "https://webapi.amap.com/maps?v=1.4.13&key=209e57a8bf80dd59da2650f389a2b6c5",
                        "//webapi.amap.com/ui/1.0/main.js"
                    ]
                }
            };
            if (process.env.NODE_ENV === "production") {
                args[0].cdn = cdn.build;
            }
            if (process.env.NODE_ENV === "development") {
                args[0].cdn = cdn.dev;
            }
            return args;
        })

    }


};